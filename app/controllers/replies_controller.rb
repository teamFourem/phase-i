class RepliesController < ApplicationController
	before_action :authenticate_user!
	before_action :find_post, only: [:create, :edit, :update, :delete]
	before_action :find_reply, only: [:edit, :update, :delete]

	def create
		@reply = @post.replies.create(reply_params)
		@reply.user_id = current_user.id

		if @reply.save
				redirect_to post_path(@post)
			else
				render 'new'
		end
	end

	def edit
	end

  def show
    @reply=Reply.find(params[:id])
  end

	def update
		if @reply.update(reply_params)
			redirect_to post_path(@post)
		else
			render "edit"
		end
	end

	def delete
		@reply.destroy
		redirect_to post_path(@post)
	end


	private

	def reply_params
		params.require(:reply).permit(:content)
	end	

	def find_post
		@post = Post.find(params[:post_id])
	end

	def find_reply
		@reply = @post.replies.find(params[:id])
	end

end
